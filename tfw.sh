#!/bin/bash
#
# ./tfw.sh OPERATION WORKSPACE
#
#
OPERATION=""
WORKSPACE=""
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m'

tf_operation () { if [[ "$1" =~ ^(plan|apply|destroy|show)$ ]]; then OPERATION=$1; return 0; else return 1; fi }

tf_workspace () { if [[ -z "$1" ]]; then terraform workspace select default; else terraform workspace select $1 || terraform workspace new $1; fi }

help () { printf "\n${RED}Invalid operation input: $1 ${NC}\n"; printf "${YELLOW}Usage: ./tfw.sh OPERATION [WORKSPACE]${NC}\n"; return; }

if ! tf_operation $1; then
  help $1
  exit
else
	tf_workspace $2
	if [[ "$3" != "test" ]]; then printf "${YELLOW}Execute \`terraform $1\` in `cat .terraform/environment` workspace${NC}\n"; fi
	terraform $OPERATION
fi

printf "${YELLOW}Reset workspace to default${NC}\n"
terraform workspace select default
