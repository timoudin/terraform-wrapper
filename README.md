**Terraform Wrapper**

Wrapper script for execution of select terraform actions with workspaces.  Simple, dirty, yet effective.

Required: Operation to perform (for now) are one of:
*  `plan`
*  `apply`
*  `destroy`
*  `show` (for testing purposes)

**Optional**: Workspace, will create Workspaces defined here or will set default workspace without input.  Will always return to default workspace after execution to avoid confusion. 

Usage: `./tfw.sh OPERATION [WORKSPACE]`
